package com.tw;



import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class ParkingLotTest {

    @Test
    public void should_get_ticket_when_parking_1_car_in_available_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car("川AE0000");
        Ticket ticket = parkingLot.park(car);
        assertNotNull(ticket);
    }


}
